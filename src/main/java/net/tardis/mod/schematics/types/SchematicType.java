package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.schematics.Schematic;

/** 
 * Extra logic object which can define additional attributes of the the Json format of {@linkplain Schematic}
 * <p> Example Usage:
 * Let's say your mod adds a number of unlockable Tardis "Model Types" which change the Tardis' behaviour like fuel capacity, speed etc.
 * If you wanted to allow your Model Type to be unlocked after the Tardis has flown a certain distance,
 * your mod would setup extra fields to allow the datapack user to populate your fields.
 * <br> Example JSON data:
 * <pre>
 * {
 *   "type:"my_mod:tardis_model_type",
 *   "display_name":"Model Type 102",
 *   "translated": false,
 *   "tardis_blocks_travelled": 600,
 *   "unlocked_model":"my_mod:tardis_model_type/one_hundred_two"
 * }
 * </pre>*/
public abstract class SchematicType extends ForgeRegistryEntry<SchematicType> {
    /**
	 *  Read the schematic's extra data from Json and return the modified Schematic back.
	 * @param root - The json object which we will extra our extra data from.
	 * @return a new Schematic instance based on data from the Schematic Type
	 */
    public abstract Schematic deserialize(JsonObject root);
    
    /**
	 * Read the schematic's extra data from a packet
	 * @param buffer - the packet buffer
	 * @return A new Schematic object to be synced to the player
	 */
	public abstract Schematic deserialize(PacketBuffer buffer);

    /**
     *  Serialize the schematic type's extra data to the packet buffer. Whatever you do here must be read from the buffer in {@link SchematicType#deserialize(PacketBuffer)}
     * @param schematic - the original Schematic object from the packet which will be modified
     * @param buffer - the packet buffer
     * @return A modified Schematic object to be used to sent to the player
     */
	public abstract Schematic serialize(Schematic schematic, PacketBuffer buffer);

	/** Serialise the schematic type's extra data to the main Schematic JSON. 
     * @param schematic - the schematic instance for this schematic type. We need the schematic so we can access its properties.
     * @return - the JsonObject which will add onto the main Schematic JSON
     */
    public abstract JsonObject serialize(Schematic schematic);

    /**
     * Set the translation key or the display name
     * @param root - the base Json object which we will be adding this extra data to
     * @param schematic - the schematic we are adding extra data to
     */
    public static void setTranslationOrDisplayName(JsonObject root, Schematic schematic){
        String name = root.get("display_name").getAsString();
        boolean translate = root.has("translated") && root.get("translated").getAsBoolean();
        schematic.setDisplayName(name);

        if(translate)
            schematic.setTranslation(name);
    }

    /**
     *  Create the root Json Object for which the rest of the Schematic Type data can be added on
     * @param displayName - the string display name or translation key for this Schematic
     * @param translated - Flag for if this display name will be used as the Translation Key for Language files
     * @return
     */
    public JsonObject createBaseJson(String displayName, boolean translated){
        JsonObject root = new JsonObject();
        root.add("type", new JsonPrimitive(this.getRegistryName().toString()));
        root.add("display_name", new JsonPrimitive(displayName));
        root.add("translated", new JsonPrimitive(translated));
        return root;
    }

    /** Get a specific Schematic instance*/
    public <T extends Schematic> T getSchematicAs(Class<T> type, Schematic schematic){
        return (T)schematic;
    }

}
