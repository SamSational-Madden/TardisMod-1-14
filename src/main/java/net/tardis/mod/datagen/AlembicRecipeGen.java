package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AlembicRecipe;
import net.tardis.mod.tags.TardisItemTags;
/**
 * Generate recipes and special recipes for Alembic
 */
public class AlembicRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public AlembicRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        createAlembicRecipeWithTag(path, cache, 1000, Ingredient.fromTag(TardisItemTags.PLANTS), 16, new ItemStack(TItems.CIRCUIT_PASTE.get(), 1));
        createFluidGenerationRecipe(path, cache, "mercury", 500, Ingredient.fromTag(TardisItemTags.CINNABAR), 1, 250);
    }

    @Override
    public String getName() {
        return "TARDIS Alembic Recipe Generator";
    }
    
    public static Path getPath(Path path, Item output) {
        ResourceLocation key = output.getRegistryName();
        return path.resolve("data/" + key.getNamespace() + "/recipes/alembic/" + key.getPath() + ".json");
    }
    
    public static Path getPathSpecial(Path path, String modID, String fileName) {
        return path.resolve("data/" + modID + "/recipes/alembic/" + fileName + ".json");
    }

    /**
     * Create an Alembic recipe using items. Make sure it is a unique recipe
     * @param path
     * @param cache
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @throws IOException 
     */
    public void createAlembicRecipeWithItem(Path path, DirectoryCache cache, int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithItem(requiredWater, ingredient, requiredIngredientCount, output), getPath(path, output.getItem()));
    }
    
    /**
     * Create an Alembic recipe using items. Make sure it is a unique recipe
     * @param path
     * @param cache
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @throws IOException 
     */
    public void createAlembicRecipeWithTag(Path path, DirectoryCache cache, int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithTag(requiredWater, ingredient, requiredIngredientCount, output), getPath(path, output.getItem()));
    }
    
    /**
     * 
     * @param base
     * @param cache
     * @param fileName - name of the file
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param resultType - the type of output result. Currently {@link AlembicRecipe.ResultType} only has ITEM and MERCURY. <br> We only use this one for MERCURY
     * @param generatedFluidAmount - the amount of fluid we are generating
     * @throws IOException
     */
    public void createFluidGenerationRecipe(Path base, DirectoryCache cache, String fileName, int requiredWater, Ingredient ingredient, int requiredIngredientCount, int generatedFluidAmount) throws IOException {
        IDataProvider.save(GSON, cache, this.createFluidGenerationRecipe(requiredWater, ingredient, requiredIngredientCount, generatedFluidAmount), getPathSpecial(base, Tardis.MODID, fileName));
    }
    
    /**
     * Creates an Alembic Recipe that outputs an item
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @return {@link JsonObject}
     */
    public JsonObject createRecipeWithItem(int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) {
        AlembicRecipe recipe = new AlembicRecipe(requiredWater, requiredIngredientCount, ingredient, output);
        return recipe.toJson();
    }
    
    /**
     * Creates an Alembic Recipe that uses tags instead of items
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. Maximum of one tag. Use {@linkplain Ingredient#fromTag} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @return {@link JsonObject}
     */
    public JsonObject createRecipeWithTag(int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) {
    	AlembicRecipe recipe = new AlembicRecipe(requiredWater, requiredIngredientCount, ingredient, output);
        return recipe.toJson();
    }
    
    /**
     * Creates a special non item recipe
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param resultType - the type of output result. Currently {@link AlembicRecipe.ResultType} only has ITEM and MERCURY. <br> We only use this one for MERCURY
     * @param resultCount - the amount of merucry/other object we want to output
     * @return {@link JsonObject}
     */
    public JsonObject createFluidGenerationRecipe(int requiredWater, Ingredient ingredient, int requiredIngredientCount, int generatedFluid) {
    	AlembicRecipe recipe = new AlembicRecipe(requiredWater, requiredIngredientCount, ingredient).setGeneratedFluid(generatedFluid);
        return recipe.toJson();
    }
    
    public JsonObject createItemFromMercuryRecipe(int requiredFluid, Ingredient ingredient, int requiredIngredientCount, AlembicRecipe.ResultType resultType, ItemStack output, int resultCount) {
    	ItemStack result = output;
    	output.setCount(resultCount);
    	AlembicRecipe recipe = new AlembicRecipe(requiredFluid, requiredIngredientCount, resultType, ingredient, result);
        return recipe.toJson();
    }

}
