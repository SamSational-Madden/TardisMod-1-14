package net.tardis.mod.damagesources;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class TEntitySource extends EntityDamageSource{

	private String message;
    private boolean bypassesArmor;

	public TEntitySource(String name, Entity damageSourceEntityIn, boolean bypassesArmor) {
		super(name, damageSourceEntityIn);
        this.message = "damagesrc.tardis." + name;
        this.bypassesArmor = bypassesArmor;
	}
	
	@Override
    public ITextComponent getDeathMessage(LivingEntity entity) {
        return new TranslationTextComponent(message, entity.getDisplayName(), damageSourceEntity.getDisplayName());
    }

    @Override
    public boolean isUnblockable() {
        return !bypassesArmor;
    }

}
