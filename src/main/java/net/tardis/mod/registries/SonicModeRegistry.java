package net.tardis.mod.registries;


import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sonic.interactions.RiftProbeInteraction;
import net.tardis.mod.sonic.interactions.SonicBlockInteraction;
import net.tardis.mod.sonic.interactions.SonicEntityInteraction;
import net.tardis.mod.sonic.interactions.SonicTardisDestinationInteraction;

import java.util.function.Supplier;

public class SonicModeRegistry {


    public static final DeferredRegister<AbstractSonicMode> SONIC_MODES = DeferredRegister.create(AbstractSonicMode.class, Tardis.MODID);
    public static Supplier<IForgeRegistry<AbstractSonicMode>> SONIC_MODE_REGISTRY = SONIC_MODES.makeRegistry("sonic_mode", () -> new RegistryBuilder<AbstractSonicMode>().setMaxID(Integer.MAX_VALUE - 1));

    public static final RegistryObject<AbstractSonicMode> BLOCK_INTERACT = SONIC_MODES.register("block_interaction", SonicBlockInteraction::new);
    public static final RegistryObject<AbstractSonicMode> ENTITY_INTERACT = SONIC_MODES.register("entity_interaction", SonicEntityInteraction::new);
//    public static final RegistryObject<AbstractSonicMode> LASER_INTERACT = SONIC_MODES.register("laser_interaction", SonicLaserInteraction::new);
    public static final RegistryObject<AbstractSonicMode> SET_DESTINATION = SONIC_MODES.register("set_destination", SonicTardisDestinationInteraction::new);
    public static final RegistryObject<AbstractSonicMode> PROBE = SONIC_MODES.register("probe", RiftProbeInteraction::new);

}
