package net.tardis.mod.misc;

import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;

import net.minecraftforge.common.crafting.NBTIngredient;

/** Created by 50ap5ud5 30/3/2021
 * <p> Allows for an {@linkplain NBTIngredient} to be serialised and deserialised to and from json
 * <br> By default, Forge does not have a codec for this
 * */
public class NBTIngredientCodec{
	/** NBTIngredient to Json only codec*/
	public static final Codec<NBTIngredient> INGREDIENT_TO_JSON_CODEC = Codec.PASSTHROUGH.comapFlatMap(
        json -> DataResult.error("Deserializing of ingredients not implemented"),
        ingredient -> new Dynamic<JsonElement>(JsonOps.INSTANCE, ingredient.serialize()));

	/**
	 * NBTIngredient from Json only Codec
	 * <br> The passthrough codec is the codec for jsons, nbt, etc. themselves
	 * <br> comapFlatMap is similar to xmap but is used for things that can't always be converted
	 */
	public static final Codec<NBTIngredient> INGREDIENT_FROM_JSON_CODEC = Codec.PASSTHROUGH.flatXmap(dynamic ->
	{
	    try
	    {
	    	NBTIngredient ingredient = NBTIngredient.Serializer.INSTANCE.parse(dynamic.convert(JsonOps.INSTANCE).getValue().getAsJsonObject());
	        return DataResult.success(ingredient);
	    }
	    catch(JsonSyntaxException e)
	    {
	        return DataResult.error(e.getMessage());
	    }
	},
	ingredient -> DataResult.error("Cannot serialize ingredients to json with this Codec!\n Use another codec for this"));
	
	/** A codec that allows {@linkplain NBTIngredient} to be Serialised and Deserialised to and from Json*/
	public static final Codec<NBTIngredient> INGREDIENT_CODEC = Codec.of(INGREDIENT_TO_JSON_CODEC, INGREDIENT_FROM_JSON_CODEC);

}
