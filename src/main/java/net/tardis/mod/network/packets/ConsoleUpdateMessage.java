package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.packets.console.ConsoleData;
import net.tardis.mod.network.packets.console.DataType;
import net.tardis.mod.network.packets.console.DataTypes;

public class ConsoleUpdateMessage {
	
	private DataType type;
	private ConsoleData data;
	
	public ConsoleUpdateMessage(DataType type, ConsoleData data) {
		this.type = type;
		this.data = data;
	}
	
	public static void encode(ConsoleUpdateMessage mes, PacketBuffer buff) {
		buff.writeString(mes.type.getRegistryId());
		
		mes.data.serialize(buff);
	}
	
	public static ConsoleUpdateMessage decode(PacketBuffer buff) {
		String id = buff.readString(TardisConstants.PACKET_STRING_LENGTH);
		DataType type = DataTypes.CONSOLE_DATA_TYPES.get(id);
		
		ConsoleData data = type.getConsoleData().get();
		data.deserialize(buff);
		
		return new ConsoleUpdateMessage(type, data);
	}
	
	public static void handle(ConsoleUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(getWorld(cont.get())).ifPresent(tile -> {
				mes.data.applyToConsole(tile, cont);
			});
		});
		cont.get().setPacketHandled(true);
	}
	
	public static World getWorld(NetworkEvent.Context context) {
		
		if(context.getDirection() == NetworkDirection.PLAY_TO_SERVER) {
			return context.getSender().getServerWorld();
		}
		
		return ClientHelper.getClientWorld();
		
	}
	

}
