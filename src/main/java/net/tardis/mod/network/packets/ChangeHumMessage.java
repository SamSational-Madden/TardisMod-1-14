package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.InteriorHum;

public class ChangeHumMessage {
    
	private InteriorHum hum;

    public ChangeHumMessage(InteriorHum name) {
        this.hum = name;
    }

    public static void encode(ChangeHumMessage mes, PacketBuffer buf) {
        buf.writeRegistryId(mes.hum);
    }

    public static ChangeHumMessage decode(PacketBuffer buf) {
        return new ChangeHumMessage(buf.readRegistryId());
    }

    public static void handle(ChangeHumMessage mes, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
        	ctx.get().getSender().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	    data.getInteriorEffectsHandler().setHum(mes.hum);
            });
        });
        ctx.get().setPacketHandled(true);
    }
}

