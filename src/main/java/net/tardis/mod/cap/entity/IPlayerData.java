package net.tardis.mod.cap.entity;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.SpaceTimeCoord;

public interface IPlayerData extends INBTSerializable<CompoundNBT> {

    SpaceTimeCoord getDestination();

    void setDestination(SpaceTimeCoord coord);

    double getDisplacement();

    void calcDisplacement(BlockPos start, BlockPos finish);

    void setShaking(int shakingTicks, float magnitude);

    int getShaking();

    void setShaking(int shake);

    void startCountdown(int time);

    int getCountdown();

    void tick();

    void update();
    /** 1.17: IStorage interface removed with no replacement. Move these methods to the provider that implements {@linkplain ICapabilitySerializable}*/
    @Deprecated
    public class Storage implements Capability.IStorage<IPlayerData> {

        @Override
        public INBT writeNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side, INBT nbt) {
            if (nbt instanceof CompoundNBT)
                instance.deserializeNBT((CompoundNBT) nbt);
        }
    }

    class Provider implements ICapabilitySerializable<CompoundNBT> {

        IPlayerData data;

        public Provider(IPlayerData data) {
            this.data = data;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.PLAYER_DATA ? (LazyOptional<T>) LazyOptional.of(() -> data) : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return data.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            data.deserializeNBT(nbt);
        }

    }
}
