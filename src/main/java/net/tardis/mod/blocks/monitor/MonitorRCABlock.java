package net.tardis.mod.blocks.monitor;

import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.properties.Prop;

public class MonitorRCABlock extends MonitorBlock {

    public MonitorRCABlock() {
        super(Prop.Blocks.BASIC_TECH.get(), TardisConstants.Gui.MONITOR_MAIN_RCA, -0.4, -0.1, -0.344, 0, 0);
    }

}
