package net.tardis.mod.containers;

import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntity;
/** Base container with context to the underlying tile entity*/
public abstract class BlockEntityContextContainer<T extends TileEntity> extends BaseContainer{
    
	protected T blockEntity;
	
	protected BlockEntityContextContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	public T getBlockEntity() {
		return this.blockEntity;
	}

}
