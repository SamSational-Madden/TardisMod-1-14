package net.tardis.mod.client.models.exteriors;// Made with Blockbench 3.9.2
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class DisguiseDoorModel extends ExteriorModel {

	private final ModelRenderer open;
	private final ModelRenderer half;

	public DisguiseDoorModel() {
		textureWidth = 16;
		textureHeight = 16;

		open = new ModelRenderer(this);
		open.setRotationPoint(0.0F, 24.0F, 0.0F);
		open.setTextureOffset(0, 0).addBox(-7.0F, -31.0F, -8.075F, 14.0F, 31.0F, 1.0F, 0.0F, false);

		half = new ModelRenderer(this);
		half.setRotationPoint(0.0F, 24.0F, 0.0F);
		half.setTextureOffset(0, 0).addBox(-5.0F, -31.0F, -8.075F, 10.0F, 31.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		open.render(matrixStack, buffer, packedLight, packedOverlay);
		//half.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		if(exterior.getOpen() == EnumDoorState.BOTH)
			open.render(matrixStack, buffer, packedLight, packedOverlay);
		else if(exterior.getOpen() == EnumDoorState.ONE)
			half.render(matrixStack, buffer, packedLight, packedOverlay);
	}
}