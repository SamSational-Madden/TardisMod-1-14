package net.tardis.mod.client.guis.monitors;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ExteriorChangeMessage;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.List;

public class ExteriorScreen extends MonitorScreen {

    private List<AbstractExterior> unlockedExteriors = Lists.newArrayList();
    private int index = 0;

    public ExteriorScreen(IMonitorGui monitor, String type) {
        super(monitor, type);
    }

    @Override
    protected void init() {
        super.init();
        this.unlockedExteriors.clear();
        ConsoleTile console = TardisHelper.getConsoleInWorld(this.minecraft.world).orElse(null);
        if (console != null) {
        	for (AbstractExterior ext : console.getUnlockManager().getUnlockedExteriors()) {
        		if (!this.unlockedExteriors.contains(ext))
        			this.unlockedExteriors.add(ext);
        	}
            
        }

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
                TardisConstants.Translations.GUI_PREV, but -> modIndex(-1)));

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
                TardisConstants.Translations.GUI_SELECT, but -> {
                    Network.sendToServer(new ExteriorChangeMessage(this.getExteriorFromIndex().getRegistryName()));
                    Minecraft.getInstance().displayGuiScreen(null);
                }));

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(),
                TardisConstants.Translations.GUI_NEXT, but -> modIndex(1)));

        int temp = 0;
        for (AbstractExterior ext : this.unlockedExteriors) {
            if (ext == console.getExteriorType())
                index = temp;
            ++temp;
        }
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);

        AbstractExterior ext = this.getExteriorFromIndex();

        if (ext != null) {
            this.minecraft.getTextureManager().bindTexture(ext.getBlueprintPreviewTexture());

            int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);

            int width = 100, height = 100;

            blit(matrixStack, centerX - width / 2, this.parent.getMaxY(), 0, 0, width, height, width, height);
        }
    }

    public int modIndex(int mod) {
        if (index + mod >= this.unlockedExteriors.size())
            return index = 0;
        else if (index + mod < 0)
            return index = this.unlockedExteriors.size() - 1;
        return this.index += mod;
    }

    public AbstractExterior getExteriorFromIndex() {
        if (index < 0 || index >= this.unlockedExteriors.size()) {
            this.index = 0;
            return ExteriorRegistry.STEAMPUNK.get();
        }
        return this.unlockedExteriors.get(index);
    }
}
