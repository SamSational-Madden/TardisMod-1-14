package net.tardis.mod.client.guis.radial;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.client.guis.RadialMenuScreen;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SonicModeChangeMessage;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sounds.TSounds;

public class SonicModeScreen extends RadialMenuScreen {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/radial/bright_blue.png");


    public SonicModeScreen(ITextComponent titleIn) {
        super(titleIn);
    }

    @Override
    public void addRadialButtons() {
        for(AbstractSonicMode mode : SonicModeRegistry.SONIC_MODE_REGISTRY.get()){
            this.addMenuButton(new ItemRadialAction(mode.getItemDisplayIcon(), mode.getTranslation(), () -> {
                Network.sendToServer(new SonicModeChangeMessage(mode, Minecraft.getInstance().player.getEntityId()));
                Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.SONIC_MODE_CHANGE.get(), 1.0F));
                Minecraft.getInstance().displayGuiScreen(null);
            }));
        }
    }

    @Override
    public ResourceLocation getTexture() {
        return TEXTURE;
    }
}
