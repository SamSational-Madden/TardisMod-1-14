package net.tardis.mod.client.guis.manual.pages;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.client.guis.manual.Chapter;
import net.tardis.mod.client.guis.manual.ManualScreen;

import java.util.ArrayList;
import java.util.List;

public class TOCPage extends Page{

    private final List<NamedAction> actions = new ArrayList<>();
    private final ManualScreen manual;

    public TOCPage(ManualScreen manual){
        this.manual = manual;
        this.addButtons(manual);
    }

    @Override
    public List<String> getLines() {
        return new ArrayList<>();
    }

    @Override
    public int getNumberOfLines() {
        return 0;
    }

    public void addButtons(ManualScreen screen){
        int index = 0;
        for(Chapter chapter : screen.getChapters()){

            if(!chapter.getDisplayName().isEmpty()){
                final int finalIndex = index;
                this.actions.add(new NamedAction(chapter.getDisplayName(), () -> {
                    screen.openPage(finalIndex, 0);
                }, Minecraft.getInstance().fontRenderer.FONT_HEIGHT * index));
                ++index;
            }
        }
    }

    @Override
    public void render(MatrixStack stack, FontRenderer font, int globalPage, int x, int y, int width, int height) {
        //super.render(stack, font, globalPage, x, y, width, height);
        for(int i = 0; i < this.actions.size(); ++i){
            NamedAction action = this.actions.get(i);
            font.drawText(stack, new StringTextComponent(action.name), x, y + action.y + font.FONT_HEIGHT, 0x000000);
        }
    }

    @Override
    public void onClick(double x, double y) {
        for(NamedAction action : this.actions){
            if(action.isInBounds((int)Math.floor(y), Minecraft.getInstance().fontRenderer.FONT_HEIGHT)){
                action.act();
                return;
            }
        }
    }

    static class NamedAction{

        private String name;
        private Runnable action;
        private int y;

        public NamedAction(String name, Runnable action, int y){
            this.name = name;
            this.action = action;
            this.y = y;
        }

        public String getName(){
            return name;
        }

        public void act(){
            this.action.run();
        }

        public boolean isInBounds(int y, int fontHeight){
            return y > this.y && y < this.y + fontHeight;
        }

    }

}
