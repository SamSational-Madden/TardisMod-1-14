package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.ProtocolRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Basic template for making new Monitor GUIS
 *
 * @implSpec Extend this to make new Monitor Sub guis
 * @implNote If you have a protocol that uses a Monitor gui, override public String getSubmenu() {} and return a name for the menu gui
 **/
public class MonitorScreen extends Screen {

    public static StringTextComponent TITLE = new StringTextComponent("Monitor");
    public static TranslationTextComponent backTranslation = new TranslationTextComponent("gui.tardis.previous");
    public static TranslationTextComponent selectTranslation = new TranslationTextComponent("gui.tardis.select");
    public static TranslationTextComponent nextTranslation = new TranslationTextComponent("gui.tardis.next");
    protected IMonitorGui parent;
    protected String menu = "main";
    protected ConsoleTile tile;
    private int id = 0;

    private MonitorScreen(ITextComponent titleIn) {
        super(titleIn);
    }

    private MonitorScreen() {
        this(TITLE);
    }

    public MonitorScreen(IMonitorGui monitor, String type) {
        this();
        this.parent = monitor;
        this.menu = type;
    }

    @Override
    protected void init() {
        super.init();

        TardisHelper.getConsoleInWorld(this.minecraft.world).ifPresent(tile -> this.tile = tile);

        id = 0;
        this.buttons.clear();

        ProtocolRegistry.PROTOCOL_REGISTRY.get().getEntries().forEach(entry -> {
            if (entry.getValue().getSubmenu().equals(menu))
                this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), entry.getKey().getLocation()));
        });

        if (this.parent instanceof Screen)
            ((Screen) this.parent).init(this.minecraft, this.width, this.height);
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        this.renderBackground(matrixStack);
        this.parent.renderMonitor(matrixStack);

        for (Widget b : this.buttons) {
            b.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);
        }
    }

    public void addSubmenu(TextComponent com, String menu, int x, int y) {

        IPressable press = (button) -> {
            this.minecraft.displayGuiScreen(new MonitorScreen(parent, menu));
        };

        TextButton button = new TextButton(x, y - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)),
                com.getString(), press);

        this.addButton(button);
        ++id;
    }

    public void addSubmenu(TextComponent com, IPressable press) {

        TextButton button = new TextButton(this.parent.getMinX(), (this.parent.getMinY() - this.getUsedHeight()) - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)),
                com.getString(), press);

        this.addButton(button);
        ++id;
    }

    public Button createButton(int x, int y, ResourceLocation key) {
        Protocol prot = ProtocolRegistry.PROTOCOL_REGISTRY.get().getValue(key);
        IPressable press = (button) -> {
            TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                Network.sendToServer(new ProtocolMessage(prot.getRegistryName()));
                prot.call(this.minecraft.world, this.minecraft.player, (ConsoleTile) te);
            }
        };
        TextButton button = new TextButton(x, y - this.getUsedHeight() - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)), "> " + (prot != null ? prot.getDisplayName(tile).getString() : "ERROR"), press);
        ++id;
        return button;
    }

    public Button createButton(int x, int y, TextComponent trans, IPressable pres) {
        TextButton button = new TextButton(x, y - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)), trans.getString(), pres);
        ++id;
        return button;
    }

    //Override this if you need the protocols to be moved up from parent.getMinY()
    public int getUsedHeight() {
        return 0;
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }


}
