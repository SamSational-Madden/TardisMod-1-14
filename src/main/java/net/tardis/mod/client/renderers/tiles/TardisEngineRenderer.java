package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.tiles.EngineModel;
import net.tardis.mod.tileentities.TardisEngineTile;

public class TardisEngineRenderer extends TileEntityRenderer<TardisEngineTile> {

    public static final EngineModel MODEL = new EngineModel();
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/engine.png");

    public TardisEngineRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }
    
    @Override
    public void render(TardisEngineTile tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        matrixStackIn.translate(0.5, 1.5, 0.5); //Translate to middle of block
        matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(180));
        MODEL.animate(tileEntityIn);
        MODEL.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
        matrixStackIn.pop();
    }

}
