package net.tardis.mod.client.renderers.entity;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.ArrowLayer;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;

public class DefaultHumanoidRenderer extends BipedRenderer<AbstractHumanoidEntity, PlayerModel<AbstractHumanoidEntity>>{

	public DefaultHumanoidRenderer(EntityRendererManager manager) {
		super(manager, new PlayerModel<AbstractHumanoidEntity>(0.0625F, false), 0.5F);
		this.addLayer(new HeldItemLayer<AbstractHumanoidEntity, PlayerModel<AbstractHumanoidEntity>>(this));
		this.addLayer(new ArrowLayer<AbstractHumanoidEntity, PlayerModel<AbstractHumanoidEntity>>(this));
		this.addLayer(new BipedArmorLayer<AbstractHumanoidEntity, PlayerModel<AbstractHumanoidEntity>, PlayerModel<AbstractHumanoidEntity>>(this, this.getEntityModel(), new PlayerModel<AbstractHumanoidEntity>(0.0625F, false)));
	}

	@Override
	public ResourceLocation getEntityTexture(AbstractHumanoidEntity human) {
		return human.getSkin();
	}

}
