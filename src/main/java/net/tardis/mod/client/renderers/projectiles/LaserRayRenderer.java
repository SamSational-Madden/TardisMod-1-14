package net.tardis.mod.client.renderers.projectiles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.helper.TRenderHelper;

public class LaserRayRenderer extends EntityRenderer<LaserEntity> {

    public LaserRayRenderer(EntityRendererManager renderManager) {
        super(renderManager);
    }

    @Override
    public void render(LaserEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int packedLightIn) {
        matrixStack.push();
        matrixStack.rotate(Vector3f.YP.rotationDegrees(MathHelper.lerp(partialTicks, entity.prevRotationYaw, entity.rotationYaw) - 90.0F));
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(MathHelper.lerp(partialTicks, entity.prevRotationPitch, entity.rotationPitch)));
        
        matrixStack.rotate(Vector3f.XP.rotationDegrees(90.0F));
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(90.0F));
        
        IVertexBuilder vertexBuilder = bufferIn.getBuffer(TRenderTypes.LASER);
        TRenderHelper.drawGlowingLine(matrixStack.getLast().getMatrix(), vertexBuilder, entity.getRayLength(), 0.05F, entity.getColor().x, entity.getColor().y, entity.getColor().z, 1F, 15728640);
        matrixStack.pop();

    }

    @Override
    public ResourceLocation getEntityTexture(LaserEntity entity) {
        return null;
    }

}
