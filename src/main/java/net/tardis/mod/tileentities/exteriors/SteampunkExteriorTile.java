package net.tardis.mod.tileentities.exteriors;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.texturevariants.TextureVariants;
import net.tardis.mod.tileentities.TTiles;

public class SteampunkExteriorTile extends ExteriorTile{

	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	
	public static final AxisAlignedBB NORTH_BOX = new AxisAlignedBB(0, -1, -0.3, 1, 1, 0);
	public static final AxisAlignedBB EAST_BOX = new AxisAlignedBB(1, -1, 0, 1.3, 1, 1);
	public static final AxisAlignedBB SOUTH_BOX = new AxisAlignedBB(0, -1, 1, 1, 1, 1.3);
	public static final AxisAlignedBB WEST_BOX = new AxisAlignedBB(-0.3, -1, 0, -0.2, 1, 1);
	
	public SteampunkExteriorTile() {
		super(TTiles.EXTERIOR_STEAMPUNK.get());
		this.setVariants(TextureVariants.STEAM);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			Direction dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
			switch(dir) {
				case EAST: return EAST_BOX;
				case SOUTH: return SOUTH_BOX;
				case WEST: return WEST_BOX;
				default: return NORTH_BOX;
			}
		}
		return NORTH_BOX;
	}
}
