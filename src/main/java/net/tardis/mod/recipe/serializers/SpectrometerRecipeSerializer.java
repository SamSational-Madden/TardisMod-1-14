package net.tardis.mod.recipe.serializers;

import javax.annotation.Nullable;

import com.google.gson.JsonObject;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.recipe.SpectrometerRecipe;

public class SpectrometerRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<SpectrometerRecipe>{

    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, JsonObject json) {
    	SpectrometerRecipe recipe = SpectrometerRecipe.deserialiseFromJson(recipeId, json);
        return recipe;
    }

    @Nullable
    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
    	SpectrometerRecipe recipe = SpectrometerRecipe.deserialiseFromPacketBuffer(recipeId, buffer);
        return recipe;
    }

    @Override
    public void write(PacketBuffer buffer, SpectrometerRecipe recipe) {
    	SpectrometerRecipe.serialiseToPacketBuffer(buffer, recipe);
    }

}
